import React, { Component } from 'react'
import { View, ScrollView, Text, TouchableOpacity, StyleSheet } from 'react-native'
import { connect } from 'react-redux'

class Summary extends Component {
    render() {
        return (
            <View style={{ width: '100%', height: '100%', alignItems: 'center', justifyContent: 'space-evenly' }}>
                <Text>Summary</Text>
                <Text>{this.props.user.email} 333 </Text>
                <Text>{this.props.user.firstName} 444 </Text>
                <Text>{this.props.user.lastName} 555 </Text>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(Summary)
