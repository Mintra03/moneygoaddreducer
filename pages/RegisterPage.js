import React, { Component } from 'react';
import { StyleSheet, Image, View, ImageBackground, Text, ScrollView } from 'react-native';
import { Button, InputItem } from '@ant-design/react-native';
import axios from 'axios';

class RegisterPage extends Component {

    state = {
        email: '',
        password: '',
        firstname: '',
        lastname: '',
    }

    userSignUp = async () => {
        if (this.state.email === '' || this.state.password === '' || this.state.firstname === '' 
        || this.state.lastname === '' || this.state.username === '' || this.state.confirmPass === '') {
            Alert.alert('Incorrect Input', 'Please do not leave input fields blanked')
        } else {
                try {
                    await axios.post('https://zenon.onthewifi.com/moneyGo/users/register', {
                        email: this.state.email,
                        password: this.state.password,
                        firstName: this.state.firstname,
                        lastName: this.state.lastname,
                    })
                    await Alert.alert('Successed', 'Signing up was successful')
                    await this.props.history.replace('/')
                } catch (error) {
                    console.log('signup res', error.response.data.errors);
                }
            }
        }

    gotoLoginPage = () => {
    this.props.history.push('/LoginPage')
    }

    render() {
        return (
            <ImageBackground source={require('./12.jpg')} style={{ width: '100%', height: '100%' }}>
                <ScrollView contentContainerStyle={styles.contentContainer}>
                    <View style={[styles.control, styles.center]}>

                        <View style={styles.container} >
                            <View style={styles.content}>

                            <Text style={styles.headertext}> Welcom to Register </Text>

                                <View style={[styles.textInput1, styles.center]}>
                                    <InputItem
                                        value={this.state.firstname}
                                        onChange={value => {
                                            this.setState({
                                                firstname: value,
                                            });
                                        }}
                                        placeholder='Firstname'
                                    />
                                </View>

                                <View style={[styles.textInput1, styles.center]}>
                                    <InputItem
                                        value={this.state.lastname}
                                        onChange={value => {
                                            this.setState({
                                                lastname: value,
                                            });
                                        }}
                                        placeholder='Lastname'
                                    />
                                </View>

                                <View style={[styles.textInput1, styles.center]}>
                                    <InputItem
                                        value={this.state.username}
                                        onChange={value => {
                                            this.setState({
                                                username: value,
                                            });
                                        }}
                                        placeholder='Username'
                                    />
                                </View>

                                <View style={[styles.textInput1, styles.center]}>
                                    <InputItem
                                        type='password'
                                        value={this.state.password}
                                        onChange={value => {
                                            this.setState({
                                                password: value,
                                            });
                                        }}
                                        placeholder='Password'
                                    />
                                </View>

                                <View style={[styles.textInput1, styles.center]}>
                                    <InputItem
                                        type='password'
                                        value={this.state.confirmPass}
                                        onChange={value => {
                                            this.setState({
                                                confirmPass: value,
                                            });
                                        }}
                                        placeholder='Confirm Pass'
                                    />
                                </View>

                                <View style={[styles.textInput1, styles.center]}>
                                    <InputItem
                                        value={this.state.email}
                                        onChange={value => {
                                            this.setState({
                                                email: value,
                                            });
                                        }}
                                        placeholder='Email'
                                    />
                                </View>

                                <View style={[styles.button, styles.center]} onPress={this.userSignUp}>
                                    <Button type='primary' onPress={this.userSignUp}
                                    activeStyle={{ backgroundColor: '#D73DDB' }} 
                                    style={styles.backgroundColor} > Register </Button>
                                </View>

                            </View>
                        </View>
                    </View>
                    <View style={[styles.button, styles.center]} onPress={this.gotoLoginPage}>
                        <Button type='primary' activeStyle={{ backgroundColor: '#D73DDB' }} 
                        style={styles.backgroundColor} onPress={this.gotoLoginPage}> {"<"}  Back to Login </Button>
                    </View>
                </ScrollView>
            </ImageBackground>

        );
    }
}

export default RegisterPage


const styles = StyleSheet.create({
    contentContainer: {
        paddingVertical: 20
    },

    container: {
        flex: 1
    },
    content: {
        flex: 1,
        flexDirection: 'column',
        padding: 30,
    },

    headertext: {
        color: '#8E26B7',
        fontSize: 24,
        fontWeight: 'bold',
        padding: 0.5,
        marginBottom: 10,
    },

    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    logo: {
        borderRadius: 150,
        width: 280,
        height: 280
    },

    textInput1: {
        backgroundColor: '#EAC8F3',
        paddingLeft: 10,
        paddingRight: 10,
        padding: 6,
        margin: 6,
    },

    button: {
        // flex: 1,
        margin: 16,
    },
    margin: {
        margin: 10,
    },

    textInput: {
        fontSize: 20,
        fontWeight: 'bold',
        paddingLeft: 100,
        paddingRight: 100,
        backgroundColor: 'white',
    },

    buttonBox: {
        color: 'pink',
    },

    backgroundColor: {
        backgroundColor: '#8613A5',
        borderColor: '#8613A5',
    },

    control: {
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        width: 330,
        height: 580,
        paddingLeft: 20,
        paddingRight: 20,
        margin: 8,
        marginTop: 30,
        marginLeft: 16,
        padding: 6,
    },

});
